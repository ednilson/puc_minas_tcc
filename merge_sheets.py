import pandas as pd
import time


YEARS = [y for y in range(2011, 2021)]


def main(xlsb_file):
    for y in YEARS:

        year = str(y)
        sheet_name='CiteScore '+year

        if y == 2020:
            # Somente a planilha 2020 tem o nome da coluna diferente
            col = "CiteScore 2020"
        else:
            col = "CiteScore"

        # Carrega o DataFrame Principal
        df = pd.read_csv('data/dataframe_main.csv')
        print(sheet_name)
        print('rows, cols:', df.shape)

        # Carrega a planilha Excel
        metrics = pd.read_excel(xlsb_file,
            engine="pyxlsb",
            sheet_name=sheet_name)

        # remocao de duplicatos
        metrics = metrics.drop_duplicates(subset='Scopus Source ID', keep='first')

        # Renomeia atributos e descarta outros
        field_names_map = {'Scopus Source ID': 'source_id', col: year}

        # DataFrame com ID e CiteScore
        cites = None
        cites = metrics[list(field_names_map.keys())].rename(columns=field_names_map)

        # Aplica o Merge
        df = pd.merge(df,cites, how='left', on='source_id')
        print('rows, cols:', df.shape)

        # Grava no mesmo CSV
        df.to_csv('data/dataframe_main.csv', index=False)
        time.sleep(2)

    # Gera um CSV de resultado
    df.to_csv('data/dataframe_merge_results.csv', index=False)

if __name__ == "__main__":

    xlsb_file = 'data/CiteScore-2011-2020-new-methodology-October-2021.xlsb'

    main(xlsb_file)
