## TCC da pós-graduação em Ciências de Dados e Big Data.

### ANÁLISE PREDITIVA DE INDICADORES DE DESEMPENHO DE PERIÓDICOS ACADÊMICOS


Trabalho de Conclusão de Curso apresentado ao Curso de Especialização em Ciência de Dados e Big Data como requisito parcial à obtenção do título de especialista.

Ednilson Gesseff

Belo Horizonte

2022
